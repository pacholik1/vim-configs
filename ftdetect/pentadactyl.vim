autocmd BufNewFile,BufRead pentadactyl.txt setlocal filetype=html

autocmd BufNewFile,BufRead pentadactyl.stackoverflow.com.txt setlocal filetype=markdown
autocmd BufNewFile,BufRead pentadactyl.github.com.txt setlocal filetype=markdown

autocmd BufNewFile,BufRead pentadactyl.mikro.mikroelektronika.cz.txt setlocal filetype=email
autocmd BufNewFile,BufRead pentadactyl.mail.google.com.txt setlocal filetype=email
