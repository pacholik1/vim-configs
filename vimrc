" set nocompatible

" SYNTAX
set number
syntax on
filetype plugin indent on
set autoindent
" set expandtab
set smarttab tabstop=4 shiftwidth=4
set modeline

" COMPLETION
" set omnifunc=lsp#complete
" set completeopt=longest,menuone
let g:SuperTabDefaultCompletionType = "context"
" YouCompleteMe
let g:ycm_python_binary_path = '/usr/bin/python3'
" let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_key_list_stop_completion = []
" Syntastic
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_python_exec = 'python3'
let g:syntastic_cs_checkers = ['code_checker']
" OmniSharp
" let g:OmniSharp_server_stdio = 1
" let g:OmniSharp_server_use_mono = 1
let g:OmniSharp_start_server = 0
let g:OmniSharp_port = 2000

" EYE CANDY
colorscheme peachpuff
" colorscheme gruvbox	" at the end
let g:gruvbox_italic = 1
set background=dark
set linespace=-2
" no icons or menu, no scrollbars, console dialogs
set guioptions=ac
set guifont=DejaVu\ Sans\ Mono\ for\ Powerline
set guicursor+=n-v-c:blinkon0
set colorcolumn=80
set hlsearch incsearch
" changing cursor in terminal
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

" STATUSLINE
" show current command
set showcmd
" ex mode completion
set wildmenu wildmode=list:longest,full
" buffer list
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_idx_mode = 1
let g:airline_powerline_fonts = 1
set laststatus=2

" BEHAVIOUR
set linebreak
" set scrolloff=10
set display+=lastline	" don't show @ when line doesn't fit
set mouse=a				" mouse scroll
" set autochdir
autocmd BufEnter * ProjectRootCD
autocmd QuickFixCmdPost *grep* cwindow	" don't close quickfix on grep
set nrformats=hex	" no oct: 07 -> 10
set autowriteall	" autosaving
" set hidden			" undo histrory saved
augroup hidden
	autocmd!
	autocmd BufReadPost * set bufhidden=hide
	autocmd BufWinEnter quickfix setlocal bufhidden=wipe nobuflisted noswapfile
augroup END
set directory=/tmp	" swap files in /tmp
augroup noh
	autocmd!
	autocmd InsertEnter * let b:_search=@/|let @/=''
	autocmd InsertLeave * let @/=get(b:,'_search','')|nohlsearch|redraw
augroup END

" FOLDING
set foldignore=
set foldmethod=marker
set foldlevelstart=99
let g:SimpylFold_docstring_preview = 1
let g:SimpylFold_fold_docstring	= 0

" PLUGIN SETTINGS
let g:licenses_copyright_holders_name = 'Pachol, Vojtěch <pacholick@gmail.com>'
"let g:licenses_authors_name = 'Pachol, Vojtěch <pacholick@gmail.com>'

let g:vimpager = {}
let g:less     = {}
" let g:less.enabled = 0
let g:vimpager.X11 = 0

let g:gitgutter_sign_added='┃'
let g:gitgutter_sign_modified='┃'
let g:gitgutter_sign_removed='◢'
let g:gitgutter_sign_removed_first_line='◥'
let g:gitgutter_sign_modified_removed='◢'

" WINDOWS {{{
"if has('win32')
"	"set guifont=Lucida_Console:h10
"	set guifont=DejaVu\ Sans\ Mono\ for\ Powerline:h10
"	scriptencoding utf-8
"	set encoding=utf-8
"	"source mswin.vim
"	set bs=2
"	fixdel
"endif
"}}}

" MAPPINGS, INCLUDES
source ~/.vim/include.vim
source ~/.vim/mappings.vim

if $TERM != "linux"
	silent execute '!~/.vim/bundle/gruvbox/gruvbox_256palette.sh'
	colorscheme gruvbox
endif
