call plug#begin('~/.vim/bundle')
" LANGUAGE SUPPORT
" WEB
Plug 'ternjs/tern_for_vim', { 'for': 'javascript' }			" js completer
Plug 'leafgarland/typescript-vim', { 'for': 'typescript' }	" ts completer
Plug 'ap/vim-css-color', { 'for': ['css', 'scss'] }
Plug 'digitaltoad/vim-pug', { 'for': ['pug', 'jade'] }		" jade highlighting
" PYTHON
" Plug 'nvie/vim-flake8', { 'for': 'python' }					" PEP8 check
Plug 'hynek/vim-python-pep8-indent', { 'for': 'python' }
" Plug 'rdnetto/YCM-Generator', { 'branch': 'stable', 'for': ['c', 'cpp'] }
" --clang-completer --system-boost --system-libclang
Plug 'Valloric/YouCompleteMe', { 
			\'do': 'python3 install.py --clang-completer --system-boost --system-libclang --cs-completer --tern-completer',
			\'for': ['c', 'cpp', 'python', 'go', 'typescript', 'javascript', 'rust'] }
Plug 'Konfekt/FastFold'
Plug 'tmhedberg/SimpylFold', { 'for': 'python' }
" C#
Plug 'OmniSharp/omnisharp-vim', { 'for': 'cs' }

" DOCS
" Plug 'gerw/vim-latex-suite', { 'for': 'tex' }
Plug 'lervag/vimtex', { 'for': 'tex' }
Plug '/usr/share/lilypond/2.18.2/vim/', { 'for': 'lilypond' }
" Plug 'dbakker/vim-rstlink', { 'for': 'rst' }
" Plug 'greyblake/vim-preview', { 'for': 'markdown' }
" Plug 'suan/vim-instant-markdown', { 'for': 'markdown' }
" Plug 'torkve/vim-markdown-extra-preview', { 'for': 'markdown' }
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }
" VIM
Plug 'tpope/vim-scriptease', { 'for': 'vim' }				" for vim plugins
" Plug 'vimperator/vimperator.vim', { 'for': 'vimperator' }
" DB
Plug 'vim-scripts/dbext.vim', { 'for': 'sql' }				" db access
" PS
Plug 'PProvost/vim-ps1', { 'for': 'ps1' }					" powershell highlighting
" CSV, TSV
Plug 'mechatroner/rainbow_csv'

" SYNTAX
Plug 'vim-syntastic/syntastic'								" syntax checking
Plug 'vim-scripts/matchit.zip'								" `%`
Plug 'tpope/vim-surround'									" quoting
Plug 'tpope/vim-commentary'									" `gcc`
Plug 'ervandew/supertab'									" `<tab>`
Plug 'christoomey/vim-titlecase'							" gt

" VCS
Plug 'tpope/vim-fugitive'									" git
Plug 'tommcdo/vim-fubitive'									" bitbucket for fugitive
Plug 'airblade/vim-gitgutter'								" git diff in sign col
" Plug 'junegunn/gv.vim'									" commit browser
" Plug 'airblade/vim-rooter'									" cd to project dir
Plug 'dbakker/vim-projectroot'

" COMMANDS
" Plug 'tpope/vim-unimpaired'									" ][
" Plug 'tommcdo/vim-ninja-feet'								" c]i)
Plug 'tpope/vim-eunuch'										" shell commands
Plug 'antoyo/vim-licenses'
" Plug 'godlygeek/tabular'
Plug 'dhruvasagar/vim-table-mode'
Plug 'tommcdo/vim-lion'										" glip=
Plug 'vim-scripts/BufOnly.vim'
Plug 'tpope/vim-repeat'										" . works for plugins
Plug 'tpope/vim-speeddating'
" Plug 'itchyny/calendar.vim' 
Plug 'christoomey/vim-sort-motion'							" sorting with gs
Plug 'tsuyoshicho/vim-pass'

" LOOK AND FEEL
Plug 'morhetz/gruvbox'										" color theme
Plug 'vim-airline/vim-airline'
Plug 'osyo-manga/vim-anzu'									" search position
" Plug 'johngrib/vim-game-code-break'							" arkanoid
call plug#end()
