" Stripped version of Tim Pope’s unimpaired.vim
" https://github.com/tpope/vim-unimpaired

" NEXT AND PREVIOUS {{{1
" noremap [a :previous<CR>
" noremap ]a :next<CR>
" noremap [A :first<CR>
" noremap ]A :last<CR>
" noremap [b :bprevious<CR>
" noremap ]b :bnext<CR>
" noremap [B :bfirst<CR>
" noremap ]B :blast<CR>
noremap [l :lprevious<CR>
noremap ]l :lnext<CR>
noremap [L :lfirst<CR>
noremap ]L :llast<CR>
" noremap [<C-L> :lpfile<CR>
" noremap ]<C-L> :lnfile<CR>
noremap [q :cprevious<CR>
noremap ]q :cnext<CR>
noremap [Q :cfirst<CR>
noremap ]Q :clast<CR>
" noremap [<C-Q> :cpfile<CR>
" noremap ]<C-Q> :cnfile<CR>
" noremap [t :tprevious<CR>
" noremap ]t :tnext<CR>
" noremap [T :tfirst<CR>
" noremap ]T :tlast<CR>
" noremap [<C-T> :ptprevious<CR>
" noremap ]<C-T> :ptnext<CR>

" LINE OPERATIONS {{{1
nnoremap <silent> [n :<C-U>call <SID>Context(1)<CR>
nnoremap <silent> ]n :<C-U>call <SID>Context(0)<CR>
xnoremap <silent> [n :<C-U>exe 'normal! gv'<Bar>call <SID>Context(1)<CR>
xnoremap <silent> ]n :<C-U>exe 'normal! gv'<Bar>call <SID>Context(0)<CR>
onoremap <silent> [n :<C-U>call <SID>ContextMotion(1)<CR>
onoremap <silent> ]n :<C-U>call <SID>ContextMotion(0)<CR>

function! s:BlankUp(count) abort
  put!=repeat(nr2char(10), a:count)
  ']+1
endfunction

function! s:BlankDown(count) abort
  put =repeat(nr2char(10), a:count)
  '[-1
endfunction

nnoremap <silent> [<Space> :<C-U>call <SID>BlankUp(v:count1)<CR>
nnoremap <silent> ]<Space> :<C-U>call <SID>BlankDown(v:count1)<CR>

function! s:Context(reverse) abort
  call search('^\(@@ .* @@\|[<=>|]\{7}[<=>|]\@!\)', a:reverse ? 'bW' : 'W')
endfunction

function! s:ContextMotion(reverse) abort
  if a:reverse
    -
  endif
  call search('^@@ .* @@\|^diff \|^[<=>|]\{7}[<=>|]\@!', 'bWc')
  if getline('.') =~# '^diff '
    let end = search('^diff ', 'Wn') - 1
    if end < 0
      let end = line('$')
    endif
  elseif getline('.') =~# '^@@ '
    let end = search('^@@ .* @@\|^diff ', 'Wn') - 1
    if end < 0
      let end = line('$')
    endif
  elseif getline('.') =~# '^=\{7\}'
    +
    let end = search('^>\{7}>\@!', 'Wnc')
  elseif getline('.') =~# '^[<=>|]\{7\}'
    let end = search('^[<=>|]\{7}[<=>|]\@!', 'Wn') - 1
  else
    return
  endif
  if end > line('.')
    execute 'normal! V'.(end - line('.')).'j'
  elseif end == line('.')
    normal! V
  endif
endfunction

" OPTION TOGGLING {{1
" noremap +b :set background!<CR>
" noremap +c :set cursorline!<CR>
" noremap +d :set diff!<CR>
" noremap +h :set hlsearch!<CR>
" noremap +i :set ignorecase!<CR>
noremap +l :set list!<CR>
noremap +n :set number!<CR>
noremap +r :set relativenumber!<CR>
noremap +s :set spell!<CR>
" noremap +u :set cursorcolumn!<CR>
" noremap +v :set virtualedit!<CR>
" noremap +w :set wrap!<CR>
" noremap +x :set cursorline! cursorcolumn!<CR>
