function! s:ColorOnRelCol(relcol)
	let ret = map(synstack(line('.'), col('.')+a:relcol), 'synIDattr(v:val,"name")')
	if type(ret) != v:t_list
		return [v:null]
	end
	return l:ret
endfunction

function! ColorMovement()
	let synnames = <SID>ColorOnRelCol(0) 
	while <SID>ColorOnRelCol(-1) == synnames
		normal h
	endwhile

	normal v

	while <SID>ColorOnRelCol(1) == synnames
		normal l
	endwhile

	" if a:ai ==# 'i'
	" 	normal hol
	" end
endfunction

onoremap io :call ColorMovement()<CR>
