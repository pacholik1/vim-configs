function! s:ExtendedHome()
    let column = col('.')
    normal! ^
    if column == col('.')
        normal! 0
    endif
endfunction

noremap <silent> 0 :call <SID>ExtendedHome()<CR>
" inoremap <silent> <Home> <C-O>:call ExtendedHome()<CR>
