setlocal expandtab
setlocal shiftwidth=2
setlocal tabstop=2

" OMNISHARP MAPPINGS
noremap <buffer> <Leader>d :OmniSharpGotoDefinition<CR>
noremap <buffer> K :OmniSharpDocumentation<CR>
noremap <buffer> <Leader>n :OmniSharpFindUsages<CR>
noremap <buffer> <Leader>r :OmniSharpRename<CR>
