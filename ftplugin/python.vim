setlocal makeprg=python3\ %

command! PyMain call append('.', [
			\ "def main():",
			\ "    pass",
			\ "",
			\ "",
			\ "if __name__ == '__main__':",
			\ "    main(*sys.argv[1:])"
			\ ])

command! IncVer !incversion.sh '/    version/' setup.py
